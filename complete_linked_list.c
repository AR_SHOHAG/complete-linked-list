#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<conio.h>

typedef struct test_struct
{
    int val;
    struct test_struct *next;
};

test_struct *head = NULL;
test_struct *curr = NULL;

test_struct* create_list(int val)
{
    printf("\nCreating list with headnode as %d\n",val);
    test_struct *ptr = (test_struct*)malloc(sizeof(test_struct));
    if(NULL == ptr){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    head = curr = ptr;
    return ptr;
}

test_struct* add_to_list(int val, bool add_to_list_first)
{
    if(NULL == head)
        return (create_list(val));

    test_struct *ptr = (test_struct*)malloc(sizeof(test_struct));
    if(NULL == ptr){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    if(add_to_list_first){
        ptr->next = head;
        head = ptr;
    }
    else{
        curr->next = ptr;
        curr = ptr;
    }
    return ptr;
}

test_struct* search_in_list(int val, test_struct **prev)
{
    test_struct *ptr = head;
    test_struct *tmp = NULL;
    bool found = false;

    while(ptr != NULL){
        if(ptr->val == val){
            found = true;
            break;
        }
        else{
            tmp = ptr;
            ptr = ptr->next;
        }
    }

    if(true == found){
        if(prev)
            *prev = tmp;
        return ptr;
    }
    else
        return NULL;
}

int delete_from_list(int val)
{
    test_struct *prev = NULL;
    test_struct *del = NULL;

    del = search_in_list(val, &prev);

    if(del == NULL)
        return -1;

    else{
        if(prev != NULL)
            prev->next = del->next;

        else if(del == head)
            head = del->next;
    }

    free(del);
    del = NULL;

    return 0;
}

void print_list()
{
    test_struct *ptr = head;

    while(ptr->next != NULL){
        printf("%d -> ", ptr->val);
        ptr = ptr->next;
    }
    if(ptr->next == NULL)
        printf("%d ",ptr->val);

    return;
}

int main()
{
    int ret = 0, n, n_first, i, i_first, key_ser, key_del;
    test_struct *ptr = NULL;

    printf("\nHow many values you want to add to the list?:  ");
    scanf("%d", &n);
    int count[n];
    for(i=0 ; i<n ; i++){
        scanf("%d", &count[n]);
        add_to_list(count[n], false);
    }
    printf("\n\n********* Creating nodes with %d values *********\n\n", n);
    print_list();

    printf("\n\nHow many values you want to add to the list from first?:  ");
    scanf("%d", &n_first);
    int count_first[n_first];
    for(i_first=0 ; i_first<n_first ; i_first++){
        scanf("%d", &count_first[n_first]);
        add_to_list(count_first[n_first], true);
    }
    printf("\n\n********* Adding %d values from first *********\n\n", n);
    print_list();

    printf("\n\nEnter the value you want to search:  ");
    scanf("%d", &key_ser);

    ptr = search_in_list(key_ser, NULL);
    if(NULL == ptr)
        printf("\n\nSearching failed, %d is not found in the list\n\n",key_ser);

    else{
        printf("\n\n%d is found in the list\n\n", key_ser);
        printf("\n\n***************** The list *******************\n\n");
    }
    print_list();

    printf("\n\nEnter the value you want to delete:  ");
    scanf("%d", &key_del);

    ret = delete_from_list(key_del);
    if(ret != 0){
        printf("\n\nDeleting failed, %d is not found in the list\n\n",key_del);
        printf("\n\n****************** The list ********************\n\n");
    }
    else{
        printf("\n\n%d is deleted \n\n",key_del);
        printf("\n\n************** After deleting %d ***************\n\n", key_del);
    }

    print_list();

    getch();
    return 0;

}
